//add class to header on scroll
$(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
        $(".fixed-top").addClass("fixed-theme");
    } else {
        $(".fixed-top").removeClass("fixed-theme");
    }
});


//on click window move top
$(document).ready(function() {
    //window scroll top
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('#movetop').fadeIn();
        } else {
            $('#movetop').fadeOut();
        }
    });
    //Click event to scroll to top
    $('#movetop').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
});